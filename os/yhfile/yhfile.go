package yhfile

import (
	"sync"
	"os"
	"fmt"
	"yhlib/service/yherror"
)

type YhMutexFile struct {
	mutex sync.Mutex
	File *os.File
}

func Create(name string) (*YhMutexFile, error) {
	file, err := os.Create(name)
	if err != nil {
		return nil, yherror.NewYhTracedError(err.Error())
	}
	return &YhMutexFile{File: file}, nil
}

func Open(name string) (*YhMutexFile, error) {
	file, err := os.Open(name)
	if err != nil {
		return nil, yherror.NewYhTracedError(err.Error())
	}
	return &YhMutexFile{File: file}, nil
}

func OpenFile(name string, flag int, perm os.FileMode) (*YhMutexFile, error) {
	file, err := os.OpenFile(name, flag, perm)
	if err != nil {
		return nil, yherror.NewYhTracedError(err.Error())
	}
	return &YhMutexFile{File: file}, nil
}

func (f *YhMutexFile) Println(data string) error {
	f.mutex.Lock()
	_, err := f.File.WriteString(data+"\n")
	f.mutex.Unlock()
	if err != nil {
		return yherror.NewYhTracedError(err.Error())
	} else {
		return nil
	}
}

func (f *YhMutexFile) Printf(format string, args... interface{}) error  {
	f.mutex.Lock()
	_, err := f.File.WriteString(fmt.Sprintf(format, args...))
	f.mutex.Unlock()
	if err != nil {
		return yherror.NewYhTracedError(err.Error())
	} else {
		return nil
	}
}

func (f *YhMutexFile) Write(data []byte) error  {
	f.mutex.Lock()
	_, err := f.File.Write(data)
	f.mutex.Unlock()
	if err != nil {
		return yherror.NewYhTracedError(err.Error())
	} else {
		return nil
	}
}

func (f *YhMutexFile) Close() error {
	return f.File.Close()
}
