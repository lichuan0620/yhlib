package yhmysql

import (
	"github.com/go-sql-driver/mysql"
	"database/sql"
	"time"
	"yhlib/service/yherror"
	"encoding/json"
)

const (
	kDefaultDriver    = "mysql"
	kDefaultNet       = "tcp"
	kDefaultCharset   = "utf8mb4"
	kDefaultParsetime = true
	kDefaultTimezone  = "Asia/Shanghai"
)

type DbSpawner struct {
	Driver string
	Config mysql.Config
}

func NewSpawner() (spw *DbSpawner, err error) {
	config := mysql.NewConfig()
	config.Net = kDefaultNet
	config.ParseTime = kDefaultParsetime
	if loc, err := time.LoadLocation(kDefaultTimezone); err != nil {
		return nil, yherror.NewYhTracedError(err.Error())
	} else {
		config.Loc = loc
	}
	config.Params = map[string]string{
		"charset": kDefaultCharset,
	}
	spw = &DbSpawner{
		Driver: kDefaultDriver,
		Config: *config,
	}
	return
}

func NewSpawnerFromJSON(data []byte) (spw *DbSpawner, err error) {
	if spw, err = NewSpawner(); err != nil {
		return nil, yherror.NewYhTracedError(err.Error())
	}
	if err = json.Unmarshal(data, spw); err != nil {
		return nil, yherror.NewYhTracedError(err.Error())
	}
	return
}

func (spw *DbSpawner) Spawn() (db *sql.DB, err error) {
	dsn := spw.Config.FormatDSN()
	if db, err = sql.Open(spw.Driver, dsn); err != nil {
		return nil, yherror.NewYhTracedError(err.Error())
	}
	if err = db.Ping(); err != nil {
		return nil, yherror.NewYhTracedError(err.Error())
	}
	return
}
