package yhsign

import (
	"github.com/gomodule/redigo/redis"
	"hash"
	"crypto/hmac"
)

type YhSigner struct {
	redis *redis.Pool
	hmac  hash.Hash
}

func EncryptHMAC(data, key string, hash func() hash.Hash) (signed string) {
	encoder := hmac.New(hash, []byte(key))
	encoder.Write([]byte(data))
	signed = string(encoder.Sum([]byte(nil)))
	return
}
