package yherror

import (
	"runtime"
	"strconv"
	"time"
	"path/filepath"
)

// YhTracedError是一个包含错误发生位置和时间信息的错误类型。其生成的错误信息会显示错误发生
// 的文件，function，和行数，方便使用者Debug。在公共API中使用时应注意避免暴露给用户不必要
// 的信息。
type YhTracedError struct {
	message  string
	function string
	file     string
	line     string
	when     time.Time
}

// 生成一个YhTracedError并在其中记录该function的调用位置
func NewYhTracedError(message string) (err error) {
	pc, file, line, _ := runtime.Caller(1)
	return &YhTracedError{
		message:  message,
		function: runtime.FuncForPC(pc).Name(),
		file:     filepath.Base(file),
		line:     strconv.Itoa(line),
		when:     time.Now(),
	}
}

// YhTracedError.Error生成错误信息并使YhTracedError满足成为error interface的条件
func (e *YhTracedError) Error() (error string) {
	return e.when.Format(time.RFC822) + " " + e.file + ":" + e.line + " (" + e.function + "): " + e.message
}
