package main

import (
	"yhlib/service/yherror"
	"fmt"
)

func main() {
	e := yherror.NewYhTracedError("message")
	fmt.Println(e.Error())
}
